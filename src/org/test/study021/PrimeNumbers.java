package org.test.study021;

import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

public class PrimeNumbers implements Task<Set<Integer>, Integer> {


    @Override
    public Set<Integer> run(Integer numbers) {
        int check;
        Set<Integer> primeNumbers = new LinkedHashSet<Integer>();
        primeNumbers.add(2);
        primeNumbers.add(3);
        primeNumbers.add(5);
        primeNumbers.add(7);
        label:
        for (int i = 10; i <= numbers; i++) {
            for (int numDigit = 2; numDigit <= 9; numDigit++) {
                if ((i % numDigit) == 0) {
                    continue label;
                }
            }
            check = (int) Math.sqrt(i);
            for (int pn : primeNumbers) {
                if ((i % pn) == 0)
                    continue label;
                if (pn >= check) {
                    primeNumbers.add(i);
                    continue label;
                }
            }
        }

        try {
            Thread.sleep(new Random().nextInt(2000));
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return primeNumbers;
    }
}
