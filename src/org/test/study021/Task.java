package org.test.study021;

public interface Task<X, Y> {
	X run(Y value);
}