package org.test.study021;

public class TaskSubmission<X, Y> {
	private Task<X, Y> task;
	private Y value;

    private X result;

	public TaskSubmission(Task<X, Y> task, Y value) {
		this.setTask(task);
		this.setValue(value);
    }

	public Task<X, Y> getTask() {
		return task;
	}

	public void setTask(Task<X, Y> task) {
		this.task = task;
	}

	public Y getValue() {
		return value;
	}

    public synchronized X getResult() {
        while (this.result == null) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private synchronized void setResult(X result) {
        this.result = result;
        this.notify();
    }

	public void setValue(Y value) {
		this.value = value;
	}

    public void calculate() {
        this.setResult(this.getTask().run(this.getValue()));
    }
}
