package org.test.study021;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TaskRunnerImpl implements TaskRunner, Runnable {

    private volatile BlockingQueue<TaskSubmission<?, ?>> tasksQueue = new LinkedBlockingQueue<TaskSubmission<?, ?>>();

    public TaskRunnerImpl(int threads) {
        for (int i = 0; i < threads; i++) {
            new Thread(this).start();
        }
    }


    @Override
    public <X, Y> X run(Task<X, Y> task, Y value) {
        TaskSubmission<X, Y> taskInfo = new TaskSubmission<X, Y>(task, value);
        try {
            tasksQueue.put(taskInfo);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return taskInfo.getResult();
    }

    @Override
    public void run() {
        while (true) {
            try {
                TaskSubmission<?, ?> taskInfo = tasksQueue.take();
                taskInfo.calculate();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
