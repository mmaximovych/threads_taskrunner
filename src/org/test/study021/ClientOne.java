package org.test.study021;

import java.util.Random;
import java.util.Set;

public class ClientOne implements Runnable {

    private TaskRunner runner;
    private Random random = new Random();

    public ClientOne(TaskRunner runner) {
        this.runner = runner;
    }

    @Override
    public void run() {
        while (true) {
            Task<Set<Integer>, Integer> task = new PrimeNumbers();
            Integer number = random.nextInt(1000);
            long start = System.currentTimeMillis();
            Set<Integer> result = runner.run(task, number);
            long end = System.currentTimeMillis();
            System.out.println(String.format("Numbers size:%d, Time consumed: %d", result.size(), end - start));
        }

    }

}
